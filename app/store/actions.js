
import { Types } from "./mutations";

export const BLEisConnect = ({ commit }) => {
    console.log(`vuex -> actions -> BLE_Connect -> ${Types.SET_BLE_CONNECT_STATE}`);
    commit(Types.SET_BLE_CONNECT_STATE, true);
};

export const BLEisDisconnect = ({ commit }) => {
    commit(Types.SET_BLE_CONNECT_STATE, false);
};

export const SetLangForApp = ({ commit }, lang) => {
    commit(Types.SET_SYSYTEM_LANG, lang);
};

export const SetPlatformType = ({ commit }, { android, ios }) => {
    console.log(`actions -> setplatformtype -> ${android}, ${ios}`);
    commit(Types.SET_PLATFORM, { android, ios});
};

export const SetBLEDevice = ({ commit }, peripheral) => {
    console.log(`actions -> setplatformtype -> ${peripheral}`);
    commit(Types.SET_BLE_PERIFERAL_DEVICE, peripheral);
};