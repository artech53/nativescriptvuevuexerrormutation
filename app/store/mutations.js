
export const Types = {
  get SET_BLE_CONNECT_STATE() { return 'SET_BLE_CONNECT_STATE' },
  get SET_SYSYTEM_LANG() { return 'SET_SYSYTEM_LANG'},

  get SET_PLATFORM() { return 'SET_PLATFORM'},

  get SET_BLE_PERIFERAL_DEVICE() { return 'SET_BLE_PERIFERAL_DEVICE'},
};

const mutations = {
  [Types.SET_BLE_CONNECT_STATE](state, connect) {
    console.log(Types.SET_BLE_CONNECT_STATE, connect);
    state.BLE.isConnected = connect;
  },

  [Types.SET_SYSYTEM_LANG](state, lang) {
    console.log(Types.SET_SYSYTEM_LANG, lang);
    state.Localise = lang;
  },

  [Types.SET_PLATFORM](state, { android, ios }) {
    
    state.isAndroid = android;
    state.isIOS = ios;
  },

  [Types.SET_BLE_PERIFERAL_DEVICE](state, peripheral) {
    
    state.BLE.Device = peripheral;
  }
};

export default mutations;