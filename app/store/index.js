import Vue from 'nativescript-vue';
import Vuex from 'vuex';

import mutations from './mutations.js'
import * as actions from './actions.js'
import * as getters from './getters'

import { Words } from "~/utils/lang";
import { Bluetooth } from "nativescript-bluetooth";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
  state: {
    Lang: Words,
    Localise: 'EN',

    isAndroid: null,
    isIOS: null,

    BLE: {
      Device: new Bluetooth(),
      isConnected: false,
    },

  },

  mutations,
  actions,
  getters,
  strict: debug,
});

Vue.prototype.$store = store;

export default store;