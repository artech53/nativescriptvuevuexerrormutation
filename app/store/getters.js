

export const GET_BLE_CONNECT_STATE = state => {
    console.log('getters -> GET_BLE_CONNECT_STATE');
    return state.BLE.isConnected;
}

export const GET_WORDS = (state) => {
    console.log(`getters -> GET_WORDS -> ${state.Lang[state.Localise]}`);
    return state.Lang[state.Localise];
}

export const GET_PLATFORMS = (state) => {
    console.log(`getters -> GET_PLATFORMS ->`);
    return { isAndroid : state.isAndroid, isIOS : state.isIOS };
}

export const GET_BLE_DEVICE = (state) => {
    console.log(`getters -> GET_BLE_DEVICE ->`);
    return state.BLE.Device;
}
