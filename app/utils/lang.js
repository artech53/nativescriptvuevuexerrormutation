'use strict'

export const Words = {
    
    ['RU']: {
        GestureTemplate: 'ШАБЛОНЫ СХВАТОВ',
        QuickParameters: 'БЫСТРЫЕ ПАРАМЕТРЫ',
        NeedConnectToSmartli: 'Перед началом работы необходимо подключиться к устройству',
        BluetoothDontEnableErrorCaption: 'Блютуз выключен. Включите блютуз для продолжения работы.'
    },

    ['EN']: {
        GestureTemplate: 'GESTURES TEMPLATES',
        QuickParameters: 'QUICK PARAMETERS',
        NeedConnectToSmartli: 'Before you begin, connect to the device work ',
        BluetoothDontEnableErrorCaption: 'Bluetooth is turned off. Turn on Bluetooth to continue.'
    }
}